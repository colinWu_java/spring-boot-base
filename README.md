# 1、介绍

本仓库是一个SpringBoot基础项目框架

主要用途是：在学习其他技术时，需要有一个基础框架，以免每次都做重复工作。

所以每次做新的测试时，都会在这个主干基础上拉分支，去做测试



# 2、分支介绍

主干代码是基础框架代码，已实现功能有：

1. 一键生成controller/service/mapper/domain/query等代码
2. 集成了Redis
3. 自定义注解+拦截器实现基于RBAC接口权限校验（配套文档：https://blog.csdn.net/wujiangbo520/article/details/122057616）
4. 全局异常处理
5. 接口统一结果返回
6. 一键生成数据库设计文档
7. 大批量数据插入数据库测试
8. 根据word模板导出word文件或者转成PDF导出
9. 根据freemarker模板文件生成word文档
10. word文档转pdf文档



其他各分支代码功能说明如下：

## 2.1、RedisDistributedLock分支

自定义注解+AOP+Redis实现分布式锁

这样的好处是：每次在业务代码中想加锁的话，就直接在方法上打一个注解就可以了，不需要在业务代码中写很多重复的代码了，非常优雅，使用方便，易于扩展，维护简单

配套文档：https://blog.csdn.net/wujiangbo520/article/details/127509897



## 2.2、MultiDataSource1分支

自定义注解+AOP优雅实现多数据源切换

使用起来非常方便简单，易于扩展，方便维护

配套文档：https://blog.csdn.net/wujiangbo520/article/details/127651949



## 2.3、InterfacePreventAttack分支

自定义注解+拦截器+Redis实现接口防刷功能

使用起来非常简单，哪个就需要做防刷处理，就打注解，不需要做防刷处理就不打，十分方便

配套文档：https://blog.csdn.net/wujiangbo520/article/details/126994293



## 2.4、CheckIdempotent分支

自定义注解+拦截器+Redis实现接口幂等性校验功能

配套文档：https://blog.csdn.net/wujiangbo520/article/details/126097426



## 2.5、RedisDistributedCache分支

自定义注解+AOP实现分布式缓存功能

配套文档：https://blog.csdn.net/wujiangbo520/article/details/127656537













# 3、总结

1. 如果有任何疑问或者Bug，欢迎给我留言
2. 希望所有知识点对你能有所帮助，如果可以给个小星星的话，那就太感谢了



