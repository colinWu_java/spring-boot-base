package ${cfg.basePath}.service.impl.${cfg.moduleName};

import ${cfg.basePath}.domain.${cfg.moduleName}.${entity};
import ${cfg.basePath}.mapper.${cfg.moduleName}.${entity}Mapper;
import ${cfg.basePath}.query.${cfg.moduleName}.${entity}Query;
import ${cfg.basePath}.service.${cfg.moduleName}.${entity}Service;
import ${cfg.basePath}.utils.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
@Transactional
@Service
@Slf4j
public class ${table.serviceName}Impl extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName}{

    @Autowired
    private ${table.mapperName} ${cfg.entityNameSmall}Mapper;

    //查询分页列表数据
    public IPage<${entity}> selectMyPage(${entity}Query query) {
        QueryWrapper<${entity}> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(query.getKeyword())) {
            //下面条件根据实际情况修改
            wrapper.and(
                i -> i.like("user_name", query.getKeyword())
                     .or().like("login_name", query.getKeyword())
            );
        }
        //排序（默认根据主键ID降序排序，根据实际情况修改）
        wrapper.orderByDesc("id");
        Page<${entity}> page = new Page<>(query.getCurrent(), query.getSize());
        return super.page(page, wrapper);
    }

    //查询分页列表数据(自己写SQL)
    public Page<${entity}> selectMySqlPage(${entity}Query query) {
        Page<${entity}> page = new Page<>(query.getCurrent(), query.getSize());
        List<${entity}> list = ${cfg.entityNameSmall}Mapper.selectMySqlPage(page, query);
        return page.setRecords(list);
    }
}