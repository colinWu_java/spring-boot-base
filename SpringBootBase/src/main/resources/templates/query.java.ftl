package ${cfg.basePath}.query.${cfg.moduleName};

import org.wujiangbo.query.BaseQuery;

/**
 * @desc ${table.comment!}-查询对象
 * @author ${author}
 * @date ${date}
 */
public class ${entity}Query extends BaseQuery{
}