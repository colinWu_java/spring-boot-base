package ${package.Service}.${cfg.moduleName};

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${cfg.basePath}.domain.${cfg.moduleName}.${entity};
import ${cfg.basePath}.query.${cfg.moduleName}.${entity}Query;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

     IPage<${entity}> selectMyPage(${entity}Query query);

     Page<${entity}> selectMySqlPage(${entity}Query query);
}