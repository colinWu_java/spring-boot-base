package ${package.Controller}.${cfg.moduleName};

import ${cfg.basePath}.domain.${cfg.moduleName}.${entity};
import ${cfg.basePath}.result.PageList;
import ${cfg.basePath}.service.${cfg.moduleName}.${entity}Service;
import ${cfg.basePath}.query.${cfg.moduleName}.${entity}Query;
import ${cfg.basePath}.result.JSONResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;
import java.util.List;

/**
 * @desc ${table.comment!} API接口
 * @author ${author}
 * @date ${date}
 */
@RestController
@RequestMapping("/${cfg.entityNameSmall}")
public class ${entity}Controller{

    @Autowired
    public ${entity}Service ${cfg.entityNameSmall}Service;

    /**
     * 新增数据到【${table.comment!}】
     * @date ${date}
     */
    @PostMapping(value="/save")
    public JSONResult save(@RequestBody ${entity} ${cfg.entityNameSmall}){
        ${cfg.entityNameSmall}Service.save(${cfg.entityNameSmall});
        return JSONResult.success(true);
    }

    /**
     * 修改【${table.comment!}】表数据
     * @date ${date}
     */
    @PostMapping(value="/update")
    public JSONResult update(@RequestBody ${entity} ${cfg.entityNameSmall}){
        ${cfg.entityNameSmall}Service.updateById(${cfg.entityNameSmall});
        return JSONResult.success(true);
    }

    /**
     * 删除【${table.comment!}】数据
     * @date ${date}
     */
    @PostMapping(value="/batchDelete")
    public JSONResult batchDelete(@RequestBody ${entity}Query query){
        //删除数据库数据
        ${cfg.entityNameSmall}Service.removeByIds(Arrays.asList(query.getIds()));
        return JSONResult.success(true);
    }

    /**
    * 根据ID查询【${table.comment!}】数据
    * @date ${date}
    */
    @GetMapping(value = "/{id}")
    public JSONResult get(@PathVariable("id")Long id){
        return JSONResult.success(${cfg.entityNameSmall}Service.getById(id));
    }

    /**
    * 查询【${table.comment!}】数据（不分页）
    * @date ${date}
    */
    @GetMapping(value = "/list")
    public JSONResult list(){
        List<${entity}> list = ${cfg.entityNameSmall}Service.list(null);
        return JSONResult.success(list);
    }

    /**
     * 查询【${table.comment!}】数据（分页）
     * @param query 查询对象
     * @return PageList 分页对象
     * @date ${date}
     */
    @PostMapping(value = "/pagelist")
    public JSONResult pagelist(@RequestBody ${entity}Query query){
        IPage<${entity}> page = ${cfg.entityNameSmall}Service.selectMyPage(query);
        return JSONResult.success(new PageList<>(page.getTotal(), page.getRecords()));
    }

    /**
     * 查询【${table.comment!}】数据（分页），自己写SQL实现
     * @param query 查询对象
     * @return PageList 分页对象
     * @date ${date}
     */
    @PostMapping(value = "/myPagelist")
    public JSONResult selectMySqlPage(@RequestBody ${entity}Query query){
        IPage<${entity}> page = ${cfg.entityNameSmall}Service.selectMySqlPage(query);
        return JSONResult.success(new PageList<>(page.getTotal(), page.getRecords()));
    }

    /**
    * 根据 QueryWrapper 对象查询【${table.comment!}】数据
    * @date ${date}
    */
    @GetMapping(value = "/queryWrapper/{objName}")
    public JSONResult queryWrapper(@PathVariable("objName") String objName){
        QueryWrapper<${entity}> queryWrapper = new QueryWrapper();
        queryWrapper.eq("", objName);
        ${entity} one = ${cfg.entityNameSmall}Service.getOne(queryWrapper);
        return JSONResult.success(one);
    }

    /***********************************************************************************
    以上代码是自动生成的
    自己写的代码放在下面
    ***********************************************************************************/
}