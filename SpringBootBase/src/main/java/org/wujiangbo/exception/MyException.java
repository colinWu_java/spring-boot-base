package org.wujiangbo.exception;

/**
 * @desc 自定义异常类
 * @author 波波老师(微信：javabobo0513)
 */
public class MyException extends RuntimeException {
    public MyException(String message){
        super(message);
    }
}
