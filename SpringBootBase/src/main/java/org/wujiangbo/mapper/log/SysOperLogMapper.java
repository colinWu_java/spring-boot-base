package org.wujiangbo.mapper.log;

import org.wujiangbo.domain.log.SysOperLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.wujiangbo.query.log.SysOperLogQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
* <p>
* 操作日志记录 Mapper接口
* </p>
*
* @author bobo(weixin:javabobo0513)
* @date 2022-11-02
*/
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {

    //查询分页列表数据
    List<SysOperLog> selectMySqlPage(Page<SysOperLog> page, @Param("query") SysOperLogQuery query);
}