package org.wujiangbo.mapper.user;

import org.wujiangbo.domain.user.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.wujiangbo.query.user.UserQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
* <p>
* 用户表 Mapper接口
* </p>
*
* @author bobo(weixin:javabobo0513)
* @date 2022-11-02
*/
public interface UserMapper extends BaseMapper<User> {

    //查询分页列表数据
    List<User> selectMySqlPage(Page<User> page, @Param("query") UserQuery query);

    //新增用户数据
    Integer addUserOne(User user);

    void insertUserBatch(@Param("userList") List<User> userList);
}