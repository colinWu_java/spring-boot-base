package org.wujiangbo.service.impl.log;

import org.wujiangbo.domain.log.SysOperLog;
import org.wujiangbo.mapper.log.SysOperLogMapper;
import org.wujiangbo.query.log.SysOperLogQuery;
import org.wujiangbo.service.log.SysOperLogService;
import org.wujiangbo.utils.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 操作日志记录 服务实现类
 * </p>
 *
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
@Transactional
@Service
@Slf4j
public class SysOperLogServiceImpl extends ServiceImpl<SysOperLogMapper, SysOperLog> implements SysOperLogService{

    @Autowired
    private SysOperLogMapper sysOperLogMapper;

    //查询分页列表数据
    public IPage<SysOperLog> selectMyPage(SysOperLogQuery query) {
        QueryWrapper<SysOperLog> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(query.getKeyword())) {
            //下面条件根据实际情况修改
            wrapper.and(
                i -> i.like("user_name", query.getKeyword())
                     .or().like("login_name", query.getKeyword())
            );
        }
        //排序（默认根据主键ID降序排序，根据实际情况修改）
        wrapper.orderByDesc("id");
        Page<SysOperLog> page = new Page<>(query.getCurrent(), query.getSize());
        return super.page(page, wrapper);
    }

    //查询分页列表数据(自己写SQL)
    public Page<SysOperLog> selectMySqlPage(SysOperLogQuery query) {
        Page<SysOperLog> page = new Page<>(query.getCurrent(), query.getSize());
        List<SysOperLog> list = sysOperLogMapper.selectMySqlPage(page, query);
        return page.setRecords(list);
    }
}