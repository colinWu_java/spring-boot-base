package org.wujiangbo.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.wujiangbo.domain.user.User;
import org.wujiangbo.query.user.UserQuery;
import org.wujiangbo.result.JSONResult;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
public interface UserService extends IService<User> {

     IPage<User> selectMyPage(UserQuery query);

     Page<User> selectMySqlPage(UserQuery query);

    JSONResult insertUser1(Integer count);

    JSONResult insertUser2(Integer count);
}