package org.wujiangbo.service.log;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.wujiangbo.domain.log.SysOperLog;
import org.wujiangbo.query.log.SysOperLogQuery;

/**
 * <p>
 * 操作日志记录 服务类
 * </p>
 *
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
public interface SysOperLogService extends IService<SysOperLog> {

     IPage<SysOperLog> selectMyPage(SysOperLogQuery query);

     Page<SysOperLog> selectMySqlPage(SysOperLogQuery query);
}