package org.wujiangbo.utils;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import lombok.extern.slf4j.Slf4j;
import org.wujiangbo.domain.user.User;
import sun.misc.BASE64Encoder;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * <p>工具类</p>
 *
 * @author 波波老师(微信 ： javabobo0513)
 * @date 2022/11/16-19:02
 */
@Slf4j
public class Tools {

    public static void main(String[] args){
        word2pdf("D:\\Demo\\离职证明.doc", "D:\\Demo\\离职证明.pdf");
    }

    /**
     * word文档转成PDF文档(注意：要使用这个word转pdf的话，运行这个代码的服务器上需要安装WPS或者office软件)
     * @param wordPath word文档路径
     * @param pdfPath pdf文档路径
     */
    public static void word2pdf(String wordPath, String pdfPath){
        File inputWord = new File(wordPath);
        File outputFile = new File(pdfPath);
        InputStream docxInputStream = null;
        OutputStream outputStream = null;
        IConverter converter = null;
        try  {
            docxInputStream = new FileInputStream(inputWord);
            outputStream = new FileOutputStream(outputFile);
            converter = LocalConverter.builder().build();
            converter.convert(docxInputStream).as(DocumentType.DOCX).to(outputStream).as(DocumentType.PDF).execute();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("word文档转成PDF文档时，发生异常：{}", e.getLocalizedMessage());
        } finally {
            //关闭资源
            try {
                if(docxInputStream != null){
                    docxInputStream.close();
                }
                if(outputStream != null){
                    outputStream.close();
                }
                if(converter != null){
                    converter.shutDown();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取指定数量的用户测试对象
     * @param count 数量
     * @return
     */
    public static List<User> getUserList(int count){
        List<User> userList = new ArrayList<>();
        User u = null;
        Date now = new Date();
        for(int i=1; i<=count; i++){
            u = new User();
            u.setAddress("湖北省武汉市江夏区香港路123号宇宙花园小区"+ i + "栋" + i + "楼" + i + "号");
            u.setAge(i);
            u.setPhone(getRandomLength(11));
            if(i % 2 ==0){
                u.setSex("男");
                u.setNationality("中国");
                u.setNation("汉族");
                u.setMajor("宇宙最强Java专业" + i);
                u.setSchool("武汉工程地质大学" + i + "号");
                u.setUdesc("我是一个Java初学者，" + i + "大家多多关照");
                u.setUsername("张三" + i + "号");
            }else{
                u.setSex("女");
                u.setNationality("大中国");
                u.setNation("苗族");
                u.setMajor("宇宙最强PHP专业" + i);
                u.setSchool("武汉工程地质软件学员" + i + "号");
                u.setUdesc("我是一个Java架构师，" + i + "你们想这样.....");
                u.setUsername("李莉莉" + i + "号");
            }
            u.setDeptid(i);
            u.setCreatetime(now);
            u.setIdcard(getRandomLength(18));
            u.setQq(getRandomLength(11));
            userList.add(u);
        }
        return userList;
    }

    //生成指定位数的随机数
    public static String getRandomLength(int length) {
        String val = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            val += String.valueOf(random.nextInt(10));
        }
        return val;
    }

    /**
     * 获得指定图片文件的base64编码数据
     * @param filePath 文件路径
     * @return base64编码数据
     */
    public static String getBase64ByPath(String filePath) {
        if(!hasLength(filePath)){
            return "";
        }
        File file = new File(filePath);
        if(!file.exists()) {
            return "";
        }
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        try {
            assert in != null;
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }

    /**
     * @desc 判断字符串是否有长度
     */
    public static boolean hasLength(String str) {
        return org.springframework.util.StringUtils.hasLength(str);
    }
}