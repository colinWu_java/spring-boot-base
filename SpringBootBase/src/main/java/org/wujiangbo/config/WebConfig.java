package org.wujiangbo.config;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.wujiangbo.interceptor.CheckPermissionInterceptor;
 
/**
 * @desc 统一拦截器配置类
 * @author 波波老师(weixin：javabobo0513)
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {
 
    @Autowired
    private CheckPermissionInterceptor checkPermissionInterceptor;
 
    //添加自定义拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(checkPermissionInterceptor).addPathPatterns("/**");
        super.addInterceptors(registry);
    }
}