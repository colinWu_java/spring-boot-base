package org.wujiangbo.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @desc MybatisPlus配置类
 * @author 波波老师(weixin：javabobo0513)
 */
@Configuration
@MapperScan("org.wujiangbo.mapper") //mapper接口扫描
@EnableTransactionManagement  //事务管理
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}