package org.wujiangbo.annotation;
 
import java.lang.annotation.*;
 
/**
 * @desc 自定义注解校验权限
 * @author 波波老师(weixin：javabobo0513)
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckPermission {
 
    /**
     * 权限字符串
     */
    String per() default "";
}