package org.wujiangbo.domain.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
@Data
@TableName("t_user")
@ApiModel(value="t_user 表对应的实体对象", description="用户表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "姓名")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "性别")
    @TableField(value = "sex")
    private String sex;

    @ApiModelProperty(value = "年龄")
    @TableField(value = "age")
    private Integer age;

    @ApiModelProperty(value = "手机号")
    @TableField(value = "phone")
    private String phone;

    @ApiModelProperty(value = "家庭地址")
    @TableField(value = "address")
    private String address;

    @ApiModelProperty(value = "归属部门ID")
    @TableField(value = "deptid")
    private Integer deptid;

    @ApiModelProperty(value = "个人描述")
    @TableField(value = "udesc")
    private String udesc;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "createtime")
    private Date createtime;

    @ApiModelProperty(value = "毕业院校")
    @TableField(value = "school")
    private String school;

    @ApiModelProperty(value = "专业名称")
    @TableField(value = "major")
    private String major;

    @ApiModelProperty(value = "国籍")
    @TableField(value = "nationality")
    private String nationality;

    @ApiModelProperty(value = "民族")
    @TableField(value = "nation")
    private String nation;

    @ApiModelProperty(value = "身份证号码")
    @TableField(value = "idcard")
    private String idcard;

    @ApiModelProperty(value = "QQ号")
    @TableField(value = "qq")
    private String qq;
}