package org.wujiangbo.domain.log;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 操作日志记录
 * </p>
 *
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
@Data
@ApiModel(value="sys_oper_log 表对应的实体对象", description="操作日志记录")
public class SysOperLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "日志主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "模块标题")
    @TableField(value = "title")
    private String title;

    @ApiModelProperty(value = "业务类型（0其它 1新增 2修改 3删除 4查询）")
    @TableField(value = "business_type")
    private Integer businessType;

    @ApiModelProperty(value = "方法名称")
    @TableField(value = "method")
    private String method;

    @ApiModelProperty(value = "请求方式")
    @TableField(value = "request_method")
    private String requestMethod;

    @ApiModelProperty(value = "操作类别（0其它 1后台用户 2手机端用户）")
    @TableField(value = "operator_type")
    private Integer operatorType;

    @ApiModelProperty(value = "操作人员")
    @TableField(value = "oper_name")
    private String operName;

    @ApiModelProperty(value = "部门名称")
    @TableField(value = "dept_name")
    private String deptName;

    @ApiModelProperty(value = "请求URL")
    @TableField(value = "oper_url")
    private String operUrl;

    @ApiModelProperty(value = "主机地址")
    @TableField(value = "oper_ip")
    private String operIp;

    @ApiModelProperty(value = "操作地点")
    @TableField(value = "oper_location")
    private String operLocation;

    @ApiModelProperty(value = "请求参数")
    @TableField(value = "oper_param")
    private String operParam;

    @ApiModelProperty(value = "返回参数")
    @TableField(value = "json_result")
    private String jsonResult;

    @ApiModelProperty(value = "操作状态（0正常 1异常）")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "错误消息")
    @TableField(value = "error_msg")
    private String errorMsg;

    @ApiModelProperty(value = "操作时间")
    @TableField(value = "oper_time")
    private LocalDateTime operTime;

    @ApiModelProperty(value = "方法执行耗时")
    @TableField(value = "take_time")
    private Long takeTime;
}