package org.wujiangbo.interceptor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.wujiangbo.annotation.CheckPermission;
import org.wujiangbo.exception.MyException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @desc 权限校验拦截器
 * @author 波波老师(微信：javabobo0513)
 */
@Component
public class CheckPermissionInterceptor implements HandlerInterceptor {

    /**
     * 前置处理
     * 该方法将在请求处理之前进行调用
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        if (!(handler instanceof HandlerMethod)) {
            //说明拦截到的请求，不是请求方法的，那就直接放行
            return true;
        }
        //指定到这里，说明请求的是方法
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        //获取方法对象
        Method method = handlerMethod.getMethod();
        //获取方法上面的 CheckPermission 注解对象
        CheckPermission methodAnnotation = method.getAnnotation(CheckPermission.class);
        if (methodAnnotation != null) {
            //获取权限字符串
            String per = methodAnnotation.per();
            if(StringUtils.isNotBlank(per)){
                /**
                 * 此时说明访问该接口，需要校验权限
                 * 那我们就需要从请求头中拿到token，然后根据token去Redis查到该用户的权限集合，看此权限集合信息中是否有注解中需要带权限串
                 *  有的话，放行，允许访问
                 *  没有的话，报错，提示用户没有权限
                 */
                //这里我们造点测试数据，假设从token中解析出当前登录用户的权限集合如下
                List<String> userPermissionList = new ArrayList<>();
                userPermissionList.add("user:addUser");
                userPermissionList.add("user:deleteUser");
                userPermissionList.add("user:updateUser");
                userPermissionList.add("user:pageList");

                //开始判断
                if(!userPermissionList.contains(per)){
                    throw new MyException("您暂无权限进行此操作");
                }
            }
        }
        //必须返回true，否则会拦截掉所有请求，不会执行controller方法中的内容了
        return true;
    }
}
