package org.wujiangbo.query;

import lombok.Data;

/**
 * @desc 基础查询模型
 * @author 波波老师(微信：javabobo0513)
 */
@Data
public class BaseQuery {

    private Long[] ids; //批量删除时，前端传来的主键ID数组集合（Long类型）

    //关键字
    private String keyword;
    //当前页
    private Integer current = 1;
    //每页显示多少条
    private Integer size = 10;
}