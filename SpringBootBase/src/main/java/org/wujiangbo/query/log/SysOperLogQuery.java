package org.wujiangbo.query.log;

import org.wujiangbo.query.BaseQuery;

/**
 * @desc 操作日志记录-查询对象
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
public class SysOperLogQuery extends BaseQuery{
}