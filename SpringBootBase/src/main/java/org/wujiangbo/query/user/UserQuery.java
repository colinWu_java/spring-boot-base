package org.wujiangbo.query.user;

import org.wujiangbo.query.BaseQuery;

/**
 * @desc 用户表-查询对象
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
public class UserQuery extends BaseQuery{
}