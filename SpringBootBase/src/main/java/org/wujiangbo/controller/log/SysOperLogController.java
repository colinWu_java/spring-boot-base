package org.wujiangbo.controller.log;

import org.wujiangbo.domain.log.SysOperLog;
import org.wujiangbo.result.PageList;
import org.wujiangbo.service.log.SysOperLogService;
import org.wujiangbo.query.log.SysOperLogQuery;
import org.wujiangbo.result.JSONResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;
import java.util.List;

/**
 * @desc 操作日志记录 API接口
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
@RestController
@RequestMapping("/operLog")
public class SysOperLogController{

    @Autowired
    public SysOperLogService operLogService;

    /**
     * 新增数据到【操作日志记录】
     * @date 2022-11-02
     */
    @PostMapping(value="/save")
    public JSONResult save(@RequestBody SysOperLog operLog){
        operLogService.save(operLog);
        return JSONResult.success(true);
    }

    /**
     * 修改【操作日志记录】表数据
     * @date 2022-11-02
     */
    @PostMapping(value="/update")
    public JSONResult update(@RequestBody SysOperLog operLog){
        operLogService.updateById(operLog);
        return JSONResult.success(true);
    }

    /**
     * 删除【操作日志记录】数据
     * @date 2022-11-02
     */
    @PostMapping(value="/batchDelete")
    public JSONResult batchDelete(@RequestBody SysOperLogQuery query){
        //删除数据库数据
        operLogService.removeByIds(Arrays.asList(query.getIds()));
        return JSONResult.success(true);
    }

    /**
    * 根据ID查询【操作日志记录】数据
    * @date 2022-11-02
    */
    @GetMapping(value = "/{id}")
    public JSONResult get(@PathVariable("id")Long id){
        return JSONResult.success(operLogService.getById(id));
    }

    /**
    * 查询【操作日志记录】数据（不分页）
    * @date 2022-11-02
    */
    @GetMapping(value = "/list")
    public JSONResult list(){
        List<SysOperLog> list = operLogService.list(null);
        return JSONResult.success(list);
    }

    /**
     * 查询【操作日志记录】数据（分页）
     * @param query 查询对象
     * @return PageList 分页对象
     * @date 2022-11-02
     */
    @PostMapping(value = "/pagelist")
    public JSONResult pagelist(@RequestBody SysOperLogQuery query){
        IPage<SysOperLog> page = operLogService.selectMyPage(query);
        return JSONResult.success(new PageList<>(page.getTotal(), page.getRecords()));
    }

    /**
     * 查询【操作日志记录】数据（分页），自己写SQL实现
     * @param query 查询对象
     * @return PageList 分页对象
     * @date 2022-11-02
     */
    @PostMapping(value = "/myPagelist")
    public JSONResult selectMySqlPage(@RequestBody SysOperLogQuery query){
        IPage<SysOperLog> page = operLogService.selectMySqlPage(query);
        return JSONResult.success(new PageList<>(page.getTotal(), page.getRecords()));
    }

    /**
    * 根据 QueryWrapper 对象查询【操作日志记录】数据
    * @date 2022-11-02
    */
    @GetMapping(value = "/queryWrapper/{objName}")
    public JSONResult queryWrapper(@PathVariable("objName") String objName){
        QueryWrapper<SysOperLog> queryWrapper = new QueryWrapper();
        queryWrapper.eq("", objName);
        SysOperLog one = operLogService.getOne(queryWrapper);
        return JSONResult.success(one);
    }

    /***********************************************************************************
    以上代码是自动生成的
    自己写的代码放在下面
    ***********************************************************************************/
}