package org.wujiangbo.controller.user;

import lombok.extern.slf4j.Slf4j;
import org.wujiangbo.domain.user.User;
import org.wujiangbo.result.PageList;
import org.wujiangbo.service.user.UserService;
import org.wujiangbo.query.user.UserQuery;
import org.wujiangbo.result.JSONResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.wujiangbo.utils.Tools;

import java.util.Arrays;
import java.util.List;

/**
 * @desc 用户表 API接口
 * @author bobo(weixin:javabobo0513)
 * @date 2022-11-02
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController{

    @Autowired
    public UserService userService;

    /**
     * 测试大批量数据插入数据库
     * 方式3：mybatisplus自带的saveBatch批量新增方法
     * @return
     */
    @GetMapping("/insertUser3/{count}")
    public JSONResult insertUser3(@PathVariable("count") Integer count){
        //获取测试用户集合数据
        List<User> userList = Tools.getUserList(count);
        long startTime = System.currentTimeMillis();
        userService.saveBatch(userList, 5000);//每次往数据库提交5000条数据
        long endTime = System.currentTimeMillis();
        log.info("方式3耗时：{}", (endTime - startTime));
        return JSONResult.success();
    }

    /**
     * 测试大批量数据插入数据库
     * 方式2：foreach标签批量插入
     * @return
     */
    @GetMapping("/insertUser2/{count}")
    public JSONResult insertUser2(@PathVariable("count") Integer count){
        return userService.insertUser2(count);
    }

    /**
     * 测试大批量数据插入数据库
     * 方式1：ExecutorType.BATCH批处理方式插入（最快）
     * @return
     */
    @GetMapping("/insertUser1/{count}")
    public JSONResult insertUser1(@PathVariable("count") Integer count){
        return userService.insertUser1(count);
    }

    /**
     * 新增数据到【用户表】
     * @date 2022-11-02
     */
    @PostMapping(value="/save")
    public JSONResult save(@RequestBody User user){
        userService.save(user);
        return JSONResult.success(true);
    }

    /**
     * 修改【用户表】表数据
     * @date 2022-11-02
     */
    @PostMapping(value="/update")
    public JSONResult update(@RequestBody User user){
        userService.updateById(user);
        return JSONResult.success(true);
    }

    /**
     * 删除【用户表】数据
     * @date 2022-11-02
     */
    @PostMapping(value="/batchDelete")
    public JSONResult batchDelete(@RequestBody UserQuery query){
        //删除数据库数据
        userService.removeByIds(Arrays.asList(query.getIds()));
        return JSONResult.success(true);
    }

    /**
    * 根据ID查询【用户表】数据
    * @date 2022-11-02
    */
    @GetMapping(value = "/{id}")
    public JSONResult get(@PathVariable("id")Long id){
        return JSONResult.success(userService.getById(id));
    }

    /**
    * 查询【用户表】数据（不分页）
    * @date 2022-11-02
    */
    @GetMapping(value = "/list")
    public JSONResult list(){
        List<User> list = userService.list(null);
        return JSONResult.success(list);
    }

    /**
     * 查询【用户表】数据（分页）
     * @param query 查询对象
     * @return PageList 分页对象
     * @date 2022-11-02
     */
    @PostMapping(value = "/pagelist")
    public JSONResult pagelist(@RequestBody UserQuery query){
        IPage<User> page = userService.selectMyPage(query);
        return JSONResult.success(new PageList<>(page.getTotal(), page.getRecords()));
    }

    /**
     * 查询【用户表】数据（分页），自己写SQL实现
     * @param query 查询对象
     * @return PageList 分页对象
     * @date 2022-11-02
     */
    @PostMapping(value = "/myPagelist")
    public JSONResult selectMySqlPage(@RequestBody UserQuery query){
        IPage<User> page = userService.selectMySqlPage(query);
        return JSONResult.success(new PageList<>(page.getTotal(), page.getRecords()));
    }

    /**
    * 根据 QueryWrapper 对象查询【用户表】数据
    * @date 2022-11-02
    */
    @GetMapping(value = "/queryWrapper/{objName}")
    public JSONResult queryWrapper(@PathVariable("objName") String objName){
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.eq("", objName);
        User one = userService.getOne(queryWrapper);
        return JSONResult.success(one);
    }

    /***********************************************************************************
    以上代码是自动生成的
    自己写的代码放在下面
    ***********************************************************************************/
}