package org.wujiangbo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wujiangbo.annotation.CheckPermission;
import org.wujiangbo.domain.user.User;
import org.wujiangbo.result.JSONResult;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>描述</p>
 *
 * @author 波波老师(微信 ： javabobo0513)
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    /**
     * 系统登录方法
     * @param user
     * @return
     */
    @PostMapping("/login")
    public JSONResult login(@RequestBody User user){
        /**
         * 1、入参校验
         * 2、根据账号查询数据库中用户信息，如不存在，就报错
         * 3、如果存在，就对比密码
         * 4、查询登录者所拥有的权限集合信息
         * 5、将权限集合信息存入Redis，UUID作为key
         * 6、UUID返回给前端（以后每次请求将此UUID放在请求头中传给后台即可，用来验证权限）
         */

        //4、查询登录者所拥有的权限集合信息
        List<String> userPermissionList = new ArrayList<>();
        userPermissionList.add("user:addUser");
        userPermissionList.add("user:deleteUser");
        userPermissionList.add("user:updateUser");
        userPermissionList.add("user:pageList");

        //5、将权限集合信息存入Redis，UUID作为key
        String UUID = java.util.UUID.randomUUID().toString();
        //redisTemplate.opsForValue().set(UUID, userPermissionList);

        //6、UUID返回给前端
        return JSONResult.success(UUID);
    }

    //新增用户接口
    @PostMapping("/addUser")
    @CheckPermission(per = "user:addUser")
    public JSONResult addUser(@RequestBody User user){
        System.out.println("添加用户成功");
        return JSONResult.success("添加用户成功");
    }

    //新增用户接口
    @PostMapping("/updateUser")
    @CheckPermission(per = "user:updateUser")
    public JSONResult updateUser(@RequestBody User user){
        System.out.println("编辑用户成功");
        return JSONResult.success("编辑用户成功");
    }
}
