package org.wujiangbo.ThreadPoolExceptionTest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * <p>线程池发生异常案例</p>
 *
 * @author 波波老师(weixin ： javabobo0513)
 */
public class ThreadPoolSubmitExceptionDemo {

    public static void main(String[] args) {
        //创建一个线程池
        ExecutorService executorService= Executors.newFixedThreadPool(1);

        //当线程池抛出异常后 execute抛出异常，其他线程继续执行新任务
        Future<?> submit = executorService.submit(new Task2());
        try {
            submit.get();
        } catch (Exception e) {
            System.out.println("发生异常了" + e.getLocalizedMessage());
        }
    }
}

//任务类
class Task2 implements  Runnable{

    @Override
    public void run() {
        System.out.println("开始执行任务了");
        int i = 1/0;//算术异常
    }
}