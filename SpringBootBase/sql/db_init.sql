DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除 4查询）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 1 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `take_time` bigint(20) NULL DEFAULT NULL COMMENT '方法执行耗时',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1413 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
      `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
      `username` varchar(100) DEFAULT NULL COMMENT '姓名',
      `sex` varchar(2) DEFAULT NULL COMMENT '性别',
      `age` int(3) DEFAULT NULL COMMENT '年龄',
      `phone` varchar(12) DEFAULT NULL COMMENT '手机号',
      `address` varchar(100) DEFAULT NULL COMMENT '家庭住址',
      `deptid` int(11) DEFAULT NULL COMMENT '归属部门ID',
      `udesc` varchar(255) DEFAULT NULL COMMENT '个人描述',
      `createtime` datetime DEFAULT NULL COMMENT '创建时间',
      `school` varchar(255) DEFAULT NULL COMMENT '毕业院校',
      `major` varchar(255) DEFAULT NULL COMMENT '专业名称',
      `nationality` varchar(255) DEFAULT NULL COMMENT '国籍',
      `nation` varchar(255) DEFAULT NULL COMMENT '民族',
      `idcard` varchar(255) DEFAULT NULL COMMENT '身份证号码',
      `qq` varchar(255) DEFAULT NULL COMMENT 'QQ号',
      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户信息表';